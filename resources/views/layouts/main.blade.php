<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('widgets.head')
    <body>
        <div class="container">
                    {{--@include("widget.breadcrumbs")--}}
            @yield('content')
        </div>
    </body>
</html>
<!doctype html>