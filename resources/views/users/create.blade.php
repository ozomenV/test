<form method="post" data-ajax="{{route('users.store')}}">
    <div class="form-group">
        <label for="recipient-name" class="col-form-label">ФИО *</label>
        <input name="name" type="text" class="form-control" required id="recipient-name">
    </div>
    <div class="form-group">
        <label for="recipient-email" class="col-form-label">Email *</label>
        <input name="email" required type="text" class="form-control" id="recipient-email">
    </div>
    <div class="form-group">
        <label for="message-company" class="col-form-label">Компания *</label>
        <select class="form-control" name="company_id" id="message-company" required>
            <option value="">Укажите компанию</option>
            @foreach($companies as $company)
                <option value="{{$company->id}}">{{$company->name}}</option>
            @endforeach
        </select>
    </div>
    {{csrf_field()}}
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Сохранить</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
    </div>
</form>