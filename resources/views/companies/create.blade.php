<form method="post" data-ajax="{{route('companies.store')}}">
    <div class="form-group">
        <label for="recipient-name" class="col-form-label">Наименование *</label>
        <input name="name" type="text" class="form-control" required id="recipient-name">
    </div>
    <div class="form-group">
        <label for="recipient-name" class="col-form-label">Quota (MB) *</label>
        <input name="quota" type="text" class="form-control" required id="recipient-name">
    </div>
    {{csrf_field()}}
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Сохранить</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
    </div>
</form>