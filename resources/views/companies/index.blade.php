@extends('layouts.main')
@section('content')
    <div>
        <nav class="navbar navbar-light bg-light">
            <div class="mr-2">
                <div class="btn-group">
                    <h1>Компании</h1>

                </div>
            </div>
            <div class="mr-2">
            <a href="{{route('home')}}" type="button" class="btn btn-primary">На главную</a>
            </div>
            <div class="mr-sm-2">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" href="{{route('companies.create')}}" data-title="Добавление">Добавить</button>
            </div>
        </nav>
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">ФИО</th>
                <th scope="col">Quota</th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
                @foreach($companies as $company)
                    <tr>
                        <th scope="row">{{$company->id}}</th>
                        <td>{{$company->name}}</td>
                        <td>{{$company->quota_formatted}}</td>
                        <td></td>
                        <td>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal" data-title="Редактирование" href="{{route('companies.edit', $company->id)}}">
                                <span class="oi oi-pencil"></span>
                            </button>
                            <button type="button" class="btn btn-danger" data-href="{{route('companies.destroy', $company->id)}}" data-toggle="modal" data-target="#confirm-delete">
                                <span class="oi oi-delete"></span>
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{$companies->links()}}
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Подтвердите действие
                </div>
                <div class="modal-body">
                    Вы действительно хотите удалить запись?
                </div>
                <div class="modal-footer">
                    <a class="btn btn-danger btn-ok">Да</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                </div>
            </div>
        </div>
    </div>
@endsection