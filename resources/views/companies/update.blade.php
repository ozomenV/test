<form method="POST" data-ajax="{{route('companies.update', [$company->id])}}">
    @method('PUT')
    <div class="form-group">
        <label for="recipient-name" class="col-form-label">Наименование *</label>
        <input value="{{$company->name}}" name="name" type="text" class="form-control" required id="recipient-name">
    </div>
    <div class="form-group">
        <label for="recipient-name" class="col-form-label">Quota (MB) *</label>
        <input value="{{$company->quota}}" name="quota" type="text" class="form-control" required id="recipient-name">
    </div>
    {{csrf_field()}}
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Сохранить</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
    </div>
</form>