<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Title</title>
    <meta name="description" lang="{{ app()->getLocale() }}" content="">
    <meta name="keywords" lang="{{ app()->getLocale() }}" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex, nofollow">

    <!-- Responsive -->
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">


    <link href="{{ URL::asset('css/app.css') }}" rel="stylesheet" type="text/css" >
    <script src="{{ URL::asset('js/app.js') }}"></script>
    <script src="{{ URL::asset('js/custom.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/localization/messages_ru.js"></script>
</head>