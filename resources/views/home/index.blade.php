@extends('layouts.main')
@section('content')
    <div>
        <nav class="navbar navbar-light bg-light">
            <div class="" >
                <h1>Пользователи</h1>
            </div>
            <div class="mr-sm-2">
                <a href="{{route('users.index')}}" class="btn btn-dark">Управление</a>
            </div>
        </nav>
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">ФИО</th>
                <th scope="col">Email</th>
                <th scope="col">Компания</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                        <th scope="row">{{$user->id}}</th>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->company->name}}</td>
                        <td>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div>
        <nav class="navbar navbar-light bg-light">
            <div class="" >
                <h1>Компании</h1>
            </div>
            <div class="mr-sm-2">
                <a href="{{route('companies.index')}}" class="btn btn-dark">Управление</a>
            </div>
        </nav>
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Наименование</th>
                <th scope="col">Quota</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($companies as $company)
                <tr>
                    <th scope="row">{{$company->id}}</th>
                    <td>{{$company->name}}</td>
                    <td>{{$company->quota_formatted}}</td>
                    <td>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div>
        <nav class="navbar navbar-light bg-light">
            <div class="" >
                <h1>Трафик</h1>
            </div>
            <div class="mr-sm-2">
                <a href="{{route('companies.index')}}" class="btn btn-dark">Управление</a>
            </div>
        </nav>
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Пользователь</th>
                <th scope="col">Компания</th>
                <th scope="col">Адрес</th>
                <th scope="col">Transferred</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($logs as $log)
                <tr>
                    <th scope="row">{{$log->id}}</th>
                    <td>{{$log->user->name}}</td>
                    <td>{{$log->company->name}}</td>
                    <td>{{$log->link}}</td>
                    <td>{{$log->transferred_formatted}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection