$(document).ready(function(){
    var csrf = $('meta[name="csrf-token"]').attr('content');
    $("#myModal").on("show.bs.modal", function(e) {
        var link = $(e.relatedTarget);
        $(this).find('.modal-title').html($(e.relatedTarget).data('title'));
        $(this).find(".modal-body").load(link.attr("href"), function() {
            $('form').validate({
                errorClass: "invalid",
                validClass: "success",

                submitHandler: function(form) {
                    var $form = $(form);
                    if( $form.data('ajax') ) {

                            var data = new FormData();
                            var name;
                            var val;
                            var type;
                            $form.find('input,textarea,select').each(function(){
                                name = $(this).attr('name');
                                val = $(this).val();
                                type = $(this).attr('type');
                                if((type != 'checkbox' && name) || (type == 'checkbox' && $(this).prop('checked') && name)) {
                                    if(type == 'file') {
                                        data.append(name, $(this)[0].files[0]);
                                    } else if(type == 'radio' && $(this).prop('checked')) {
                                        data.append(name, val);
                                    } else if(type != 'radio') {
                                        data.append(name, val);
                                    }
                                }
                            });
                            var request = new XMLHttpRequest();
                            request.open($form.attr('method'), $form.data('ajax'));
                            request.onreadystatechange = function() {
                                var status;
                                var resp;
                                if (request.readyState == 4) {
                                    status = request.status;
                                    resp = request.response;
                                    resp = jQuery.parseJSON(resp);
                                    if (status == 200) {
                                        if( resp.success ) {
                                            if (!resp.noclear) {
                                                $form.find('input').each(function(){
                                                    if( $(this).attr('type') != 'hidden' && $(this).attr('type') != 'checkbox' ) {
                                                        $(this).val('');
                                                    }
                                                });
                                                $form.find('textarea').val('');
                                            }
                                            if (resp.clear) {
                                                for(var i = 0; i < resp.clear.length; i++) {
                                                    $('input[name="' + resp.clear[i] + '"]').val('');
                                                    $('textarea[name="' + resp.clear[i] + '"]').val('');
                                                }
                                            }
                                            if (resp.insert && resp.insert.selector && resp.insert.html) {
                                                $(resp.insert.selector).html(resp.insert.html);
                                            }
                                            if ( resp.response ) {
                                                new Noty({
                                                    text: resp.response,
                                                    layout: 'bottomRight',
                                                    theme: 'bootstrap-v4',
                                                    type: 'success'
                                                }).show();
                                            }
                                        } else {
                                            if ( resp.response ) {
                                                new Noty({
                                                    text: resp.response,
                                                    layout: 'bottomRight',
                                                    theme: 'bootstrap-v4',
                                                    type: 'error'
                                                }).show();
                                            }
                                        }
                                        if( resp.redirect ) {
                                            if(window.location.href == resp.redirect) {
                                                window.location.reload();
                                            } else {
                                                window.location.href = resp.redirect;
                                            }
                                        }
                                        if( resp.reload ) {
                                            window.location.reload();
                                        }
                                    } else {
                                        alert('Something went wrong.');
                                    }
                                }

                            };
                            request.setRequestHeader('X-CSRF-TOKEN', csrf);
                            request.send(data);
                            return false;
                        }
                }
            })
        });
    });

    $('#confirm-delete').on('show.bs.modal', function(e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });


    $('#confirm-delete').on('click', '.btn-ok', function(e) {
        var href = $(this).attr('href');


        var request = new XMLHttpRequest();
        request.open('DELETE', href);
        request.onreadystatechange = function() {
            if (request.readyState == 4) {
                window.location.reload();
            }
        }
        request.setRequestHeader('X-CSRF-TOKEN', csrf);
        request.send();
        return false;
    });
})