<?php

namespace App\Http\Controllers;

use App\Models\Companies;
use App\Models\Log;
use App\Models\Users;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Users::getList(3);
        $companies = Companies::getList(3);
        $logs = Log::getList(3);
        return view('home.index',
            [
                'users' => $users,
                'companies' => $companies,
                'logs' => $logs,
            ]);
    }
}
