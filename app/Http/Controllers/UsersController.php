<?php

namespace App\Http\Controllers;

use App\Http\Requests\UsersRequest;
use App\Models\Companies;
use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Users::getList();
        return view('users.index',
            [
                'users' => $users
            ]);
    }

    public function create()
    {
        $companies = Companies::getList();
        return view('users.create', [
            'companies' => $companies
        ]);
    }

    public function store(Request $request)
    {
        $messages = [
            'required' => 'Поле :attribute обязательное для заполнения',
            'unique' => ':attribute должен быть уникальным',
        ];


        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => [
                'required',
                Rule::unique('users')
            ],
            'company_id' => 'required|integer'
        ], $messages);

        if($validator->fails()){
            $errors = $validator->errors();
            return [
                'success' => false,
                'response' => $errors->first()
            ];
        }
        $form = new Users();
        $form->fill($request->all());
        try {
            $form->save();
            return [
                'success' => true,
                'reload' => true
            ];

        } catch (\Exception $exception) {

            return [
                'success' => false,
                'response' => __('feedback::general.message-false'),
            ];

        };
    }

    public function edit(Users $user)
    {

        $companies = Companies::getList();
        return view('users.update', [
            'user' => $user,
            'companies' => $companies
        ]);
    }

    public function update(Request $request, Users $user)
    {
        $messages = [
            'required' => 'Поле :attribute обязательное для заполнения',
            'unique' => ':attribute должен быть уникальным',
        ];


        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => [
                'required',
                Rule::unique('users')->whereNot('email', $user->email)
            ],
            'company_id' => 'required|integer'
        ], $messages);

        if($validator->fails()){
            $errors = $validator->errors();
            return [
                'success' => false,
                'response' => $errors->first()
            ];
        }

        $user->fill($request->input());
        try {
            $user->save();
            return [
                'success' => true,
                'reload' => true
            ];

        } catch (\Exception $exception) {

            return [
                'success' => false,
                'response' => __('feedback::general.message-false'),
            ];

        };
    }

    public function destroy(Users $user)
    {
        $user->forceDelete();
        return [
            'success' => true
        ];
    }
}
