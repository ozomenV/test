<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompaniesRequest;
use App\Models\Companies;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CompaniesController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Companies::getList();
        return view('companies.index',
            [
                'companies' => $companies
            ]);
    }

    public function create()
    {
        $companies = Companies::getList();
        return view('companies.create', [
            'companies' => $companies
        ]);
    }

    public function store(Request $request)
    {
        $messages = [
            'required' => 'Поле :attribute обязательное для заполнения',
        ];


        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'quota' => 'required'
        ], $messages);

        if($validator->fails()){
            $errors = $validator->errors();
            return [
                'success' => false,
                'response' => $errors->first()
            ];
        }
        $form = new Companies();
        $form->fill($request->all());
        try {
            $form->save();
            return [
                'success' => true,
                'reload' => true
            ];

        } catch (\Exception $exception) {

            return [
                'success' => false,
                'response' => __('feedback::general.message-false'),
            ];

        };
    }

    public function edit(Companies $company)
    {
        return view('companies.update', [
            'company' => $company,
        ]);
    }

    public function update(Request $request, Companies $company)
    {
        $messages = [
            'required' => 'Поле :attribute обязательное для заполнения',
        ];


        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'quota' => 'required'
        ], $messages);

        if($validator->fails()){
            $errors = $validator->errors();
            return [
                'success' => false,
                'response' => $errors->first()
            ];
        }

        $company->fill($request->input());
        try {
            $company->save();
            return [
                'success' => true,
                'reload' => true
            ];

        } catch (\Exception $exception) {

            return [
                'success' => false,
                'response' => __('feedback::general.message-false'),
            ];

        };
    }

    public function destroy(Companies $company)
    {
        $company->forceDelete();
        return [
            'success' => true
        ];
    }
}
