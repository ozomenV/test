<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

    /**
     * App\Models\Users
     *
     * @property int $id
     * @property string $name
     * @property string $email
     * @property int $company_id
     * @property-read Companies $company
     * @property \Carbon\Carbon|null $created_at
     * @property \Carbon\Carbon|null $updated_at
     */

class Users extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';


    protected $fillable = ['name', 'email', 'company_id'];

    public function company()
    {
        return $this->hasOne(Companies::class, 'id', 'company_id');
    }

    public static function getList($limit = 10)
    {
        $result = Users::with('company');
        $result->latest();
        $result = $result->paginate($limit);
        return $result;
    }
}
