<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Log
 *
 * @property int $id
 * @property int $company_id
 * @property int $user_id
 * @property string $link
 * @property double $transferred
 * @property-read double transferred_formatted
 * @property-read Companies $company
 * @property-read Users $user
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */

class Log extends Model
{
    protected $table = 'log';


    public function company()
    {
        return $this->hasOne(Companies::class, 'id', 'company_id');
    }

    public function user()
    {
        return $this->hasOne(Users::class, 'id', 'user_id');
    }

    public function getTransferredFormattedAttribute()
    {
        return $this->transferred  . ' МБ';
    }

    public static function getList($limit = 10)
    {
        $result = Log::latest();
        $result = $result->paginate($limit);
        return $result;
    }


}
