<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Users
 *
 * @property int $id
 * @property string $name
 * @property double $quota
 * @property-read double quota_formatted
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */

class Companies extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'companies';


    protected $fillable = ['name', 'quota'];


    public static function getList($limit = 10)
    {
        $result = Companies::latest();
        $result = $result->paginate($limit);
        return $result;
    }

    public function getQuotaFormattedAttribute()
    {
        return number_format((($this->quota / 1000) / 1000), 1) . ' ТБ';
    }
}
