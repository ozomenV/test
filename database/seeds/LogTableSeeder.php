<?php

use Illuminate\Database\Seeder;

class LogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\Models\Users::getList();
        foreach ($users as $user) {
            for($i = 0; $i < 500; $i++){
                $log = new  \App\Models\Log;
                $faker = Faker\Factory::create();
                $log->link = $faker->url;
                $log->company_id = $user->company_id;
                $log->user_id = $user->id;
                $log->transferred = rand(10, (int) 100);
                $log->save();
            }
        }

    }
}
