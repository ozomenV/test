<?php

use Illuminate\Database\Seeder;
use \App\Models\Companies;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = Companies::getList();
        for($i = 0; $i < 500; $i++){
            $user = new \App\Models\Users();
            $faker = Faker\Factory::create();
            $user->name = $faker->name;
            $user->email = $faker->email;
            $user->company_id = rand($companies->min('id'), $companies->max('id'));
            $user->save();
        }
    }
}
