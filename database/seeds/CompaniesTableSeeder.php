<?php

use Illuminate\Database\Seeder;
use \App\Models\Companies;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 20; $i++){
            $company = new Companies();
            $faker = Faker\Factory::create();
            $company->name = $faker->company;
            $company->quota = rand(10000, (int) 100000000);
            $company->save();
        }
    }
}
